#ifndef __INT_AVG
#define __INT_AVG

/** @return The average of two integers @ref a and @ref b. */
int integer_avg(int a, int b);

#endif //__INT_AVG